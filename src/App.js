import React, { Component } from 'react';
import icon from './icon.svg';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import ThreadManager from './ThreadManager';
import Boards from './Boards';
import Catalog from './Catalog';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

class App extends Component {
    threadManager = null;

    onThreadSelected = (t) => {
        console.log(t, this.threadManager);
        this.threadManager.loadThread(t);
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={icon} className="App-logo" alt="icon" />
                </header>
                <div className="App-content">
                    <div className="Boards-wrapper">
                        <Boards />
                    </div>
                    <div className="Catalog-wrapper">
                        <Switch>
                            <Route exact path='/' />
                            <Route path='/catalog/:id'
                                   render={({props, match}) =>
                                       <Catalog {...props} onThreadSelected={this.onThreadSelected} match={match} />} />
                        </Switch>
                    </div>
                    <div className="Thread-wrapper">
                        <ThreadManager innerRef={el => this.threadManager = el} />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;