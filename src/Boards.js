import React, { Component } from 'react';
import './App.css';
import { NavLink } from 'react-router-dom';
import List, { ListItem } from 'material-ui/List';
import { withStyles } from 'material-ui/styles';

export const boards = [
    {
        name: '東方裏',
        url: 'https://dec.2chan.net/55/'
    },
    {
        name: 'webm',
        url: 'https://may.2chan.net/webm/'
    },
    {
        name: '虹裏may',
        url: 'https://may.2chan.net/b/'
    },
    {
        name: 'img',
        url: 'https://img.2chan.net/b/'
    }
];

const styles = {
    active: {
        backgroundColor: 'rgba(0, 0, 0, 0.12)',
    },
};

class Boards extends Component {
    render() {
        const { classes } = this.props;
        const boardsDom = boards.map((board, i) => {
            return (
                <List key={board.url}>
                    <ListItem component={NavLink} to={`/catalog/${i}`} activeClassName={classes.active} button>{board.name}</ListItem>
                </List>
            );
        });
        return (
            <List>
                {boardsDom}
            </List>
        );
    }
}

export default withStyles(styles)(Boards);