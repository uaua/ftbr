import React, { Component } from 'react';
import './App.css';
import { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import request from 'superagent';
import { boards } from './Boards';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';
import Refresh from 'material-ui-icons/Refresh';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import TextField from 'material-ui/TextField';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import { AutoSizer, List } from 'react-virtualized';
import 'react-virtualized/styles.css';

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
    },
    wrapper: {
        position: 'absolute',
        left: 380,
        bottom: 10
    },
    fab: {
        color: green[500],
        position: 'absolute',
        left: -3,
        top: -3,
        zIndex: 1,
        height: 12,
        width: 12
    },
    thumbnail: {
        width: 60,
        height: 60,
    },
    list: {
        paddingTop: 4,
        paddingBottom: 4,
        cursor: 'pointer',
        height: 60
    },
    text: {
        fontWeight: 500,
        fontSize: 14
    },
    header: {
        display: 'flex',
        flexDirection: 'column',
        height: 68,
        flexShrink: 0
    },
    content: {
        flexDirection: 'column',
        height: '100vh',
        overflowY: 'auto',
        overflowX: 'hidden',
        display: 'flex'
    },
    refreshButton: {
        width: 36,
        height: 36
    },
    search: {
        marginTop: 0
    }
};

class Catalog extends Component {
    state = {
        threads: [],
        search: '',
        order: 0,
        loading: false
    };

    getThreads(url, order) {
        return new Promise((resolve, reject) => {
            request
                .post('http://localhost:4567/tintin')
                .send({url: url, order: order})
                .set('Content-Type', 'application/json')
                .end((err, res) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        resolve(JSON.parse(res.text));
                    }
                });
        });
    }

    updateCatalog = () => {
        if (!this.state.loading) {
            this.setState(
                {
                    loading: true
                },
                () => {
                    this.getThreads(boards[this.props.match.params.id].url, this.state.order)
                        .then((data) => {
                            this.setState({
                                threads: data,
                                loading: false
                            });
                        });
                }
            )
        }
    };

    componentDidMount() {
        this.updateCatalog();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.params.id !== prevProps.match.params.id ||
            this.state.order !== prevState.order) {
            this.updateCatalog();
        }
    }

    onThreadSelected = (t) => {
        this.props.onThreadSelected(t);
    };

    searchChange = (e) => {
        this.setState({search: e.target.value});
    };

    orderChange = (e) => {
        console.log(e.target.value);
        this.setState({order: e.target.value});
    };

    render() {
        const { classes } = this.props;
        const { search, loading } = this.state;
        const threads = this.state.threads.filter((t) => {
            if (search.length !== 0) {
                return t.head_letters.toLowerCase().indexOf(search) >= 0;
            }
            return true;
        });
        const rowRenderer = ({key, index, style}) => {
            const t = threads[index];
            return (
                <ListItem style={style} key={key} onClick={() => this.onThreadSelected(t)} className={classes.list} button>
                    <Avatar
                        alt="gazo"
                        src={t.thumbnail.uri} />
                    <ListItemText
                        classes={{
                            text: classes.text
                        }}
                        primary={t.head_letters}
                        secondary={t.n_posts} />
                </ListItem>
            );
        };
        return (
            <div className={classes.root}>
                <div className={classes.header}>
                    <TextField
                        label="Search"
                        margin="normal"
                        placeholder="Search"
                        value={this.state.search}
                        className={classes.search}
                        onChange={this.searchChange} />
                    <Select
                        value={this.state.order}
                        onChange={this.orderChange}
                        displayEmpty
                    >
                        <MenuItem value={0}>通常</MenuItem>
                        <MenuItem value={1}>新順</MenuItem>
                        <MenuItem value={2}>古順</MenuItem>
                        <MenuItem value={3}>多順</MenuItem>
                        <MenuItem value={4}>少順</MenuItem>
                    </Select>
                </div>
                <div className={classes.content}>
                    <AutoSizer>
                        {({ height, width }) => {
                            return (
                                <List
                                    height={height}
                                    rowCount={threads.length}
                                    rowHeight={60}
                                    rowRenderer={rowRenderer}
                                    width={width}
                                />);
                        }}
                    </AutoSizer>
                </div>
                <div className={classes.wrapper}>
                    <Button fab color="primary" className={classes.refreshButton} onClick={this.updateCatalog}>
                        <Refresh />
                    </Button>
                    {loading && <CircularProgress className={classes.fab} size={42} />}
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Catalog);