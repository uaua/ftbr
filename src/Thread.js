import React, { Component } from 'react';
import './App.css';
import Button from 'material-ui/Button';
// import List, { ListItem } from 'material-ui/List';
import Card, { CardContent } from 'material-ui/Card';
import request from 'superagent';
import ReactTooltip from 'react-tooltip'
import green from 'material-ui/colors/green';
import Refresh from 'material-ui-icons/Refresh';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';
import { AutoSizer, List } from 'react-virtualized';
import 'react-virtualized/styles.css';
import CellMeasurer, {
    CellMeasurerCache,
} from 'react-virtualized/dist/commonjs/CellMeasurer';

const styles = theme => ({
    card: {
        width: '100vw',
        overflowY: 'hidden'
    },
    cardContent: {
        paddingBottom: 4,
    },
    list: {
        paddingBottom: 0,
    },
    root: {
        display: 'flex',
        flex: '1 1 auto',
        overflowY: 'hidden'
    }
});

class Thread extends Component {
    _cache = new CellMeasurerCache({ defaultHeight: 85, fixedWidth: true });
    _mostRecentWidth = 0;
    _resizeAllFlag = false;
    state = {
        loading: false
    };

    componentDidUpdate(prevProps, prevState) {
        // console.log("props", prevProps, this.props);
        if (prevProps.uri !== this.props.uri) {
            this.loadThread(this.props);
        }
        if (this._resizeAllFlag) {
            this._resizeAllFlag = false;
            this._cache.clearAll();
            if (this._list) {
                this._list.recomputeRowHeights();
            }
        }
    }

    _resizeAll = () => {
        this._resizeAllFlag = false;
        this._cache.clearAll();
        if (this._list) {
            this._list.recomputeRowHeights();
        }
    };

    static formatImageUrl(url) {
        return url.split('/').pop();
    }

    static formatDate(date) {
        return new Date(date).toLocaleString();
    }

    render() {
        const { classes, posts } = this.props;
        const { loading } = this.state;

        const ids = posts.filter((p) => {
            return p.id;
        }).reduce((d, p) => {
            if (!(p.id in d.s)) {
                d.s[p.id] = 0;
            }
            d.s[p.id] += 1;
            d.i[p.no] = d.s[p.id];
            return d;
        }, {i:{}, s:{}});

        const ips = posts.filter((p) => {
            return p.ip;
        }).reduce((d, p) => {
            if (!(p.ip in d.s)) {
                d.s[p.ip] = 0;
            }
            d.s[p.ip] += 1;
            d.i[p.no] = d.s[p.ip];
            return d;
        }, {i:{}, s:{}});
        // console.log(ids);

        // console.log(posts);

        let rowRenderer = ({key, index, style, parent}) => {
            const i = index;
            const p = posts[i];
            return (
                <CellMeasurer
                    cache={this._cache}
                    columnIndex={0}
                    key={key}
                    parent={parent}
                    rowIndex={index}
                    width={this._mostRecentWidth}
                >
                    {({ measure }) => (
                        <Card key={key} style={style} className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <div>
                                    <span><b>{i}</b></span>
                                    <span>{p.title}</span>
                                    <span><b> 名前:</b>{p.name}</span>
                                    {p.mailto && <span><b>mail:</b>{p.mailto}</span>}
                                    {p.ip &&
                                    <span>
                                            <b>IP:</b>
                                            <a data-for={`i${p.no}`} data-tip data-event='click focus'>
                                                <span>
                                                    {p.ip}
                                                    <span>({ips.i[p.no]}/{ips.s[p.ip]})</span>
                                                </span>
                                            </a>
                                            <ReactTooltip id={`i${p.no}`} globalEventOff='click' getContent={() => {
                                                return (
                                                    <p>{p.no}</p>
                                                );
                                            }} />
                                        </span>
                                    }
                                    {p.id &&
                                    <span><b> ID:</b>{p.id}<span>({ids.i[p.no]}/{ids.s[p.id]})</span></span>
                                    }
                                    <span><b> 投稿日:</b>{Thread.formatDate(p.date)}</span>
                                    <span><b> No.</b>{p.no}</span>
                                    {p.soudane > 0 && <span><b> そうだねx</b>{p.soudane}</span>}
                                    {p.image.uri &&
                                    <span>
                                            <b> gazo:</b>
                                            <a href={p.image.uri} target="_blank">
                                                {Thread.formatImageUrl(p.image.uri)}
                                            </a>
                                        </span>
                                    }
                                </div>
                                <div>
                                    {p.image.thumbnail &&
                                    <div>
                                        <img
                                            src={p.image.thumbnail.uri}
                                            alt="gazo"
                                            onLoad={measure}
                                        />
                                    </div>
                                    }
                                    <p dangerouslySetInnerHTML={{__html: p.body}} />
                                </div>
                            </CardContent>
                        </Card>
                    )}
                </CellMeasurer>);
        };

        return (
            <div className={classes.root}>
                <AutoSizer>
                    {({ height, width }) => {
                        if (this._mostRecentWidth !== width) {
                            this._resizeAllFlag = true;
                            this._resizeAll();
                        }
                        return (
                            <List
                                height={height}
                                rowCount={posts.length}
                                rowHeight={this._cache.rowHeight}
                                rowRenderer={rowRenderer}
                                width={width}
                                innerRef={el => this._list = el}
                            />);
                    }}
                </AutoSizer>
            </div>
        );
    }
}

Thread = withStyles(styles)(Thread);
export default Thread;