import React, { Component } from 'react';
import './App.css';
import Button from 'material-ui/Button';
import request from 'superagent';
import green from 'material-ui/colors/green';
import Refresh from 'material-ui-icons/Refresh';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';
import 'react-virtualized/styles.css';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Thread from './Thread';
import update from 'immutability-helper';

const styles = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    threadContent: {
        height: '100vh',
        display: 'flex',
        flex: 1
    },
    wrapper: {
        margin: theme.spacing.unit,
        right: 20,
        bottom: 10,
        position: 'absolute'
    },
    fab: {
        color: green[500],
        position: 'absolute',
        left: -3,
        top: -3,
        zIndex: 1,
        height: 12,
        width: 12
    },
    refreshButton: {
        width: 36,
        height: 36
    },
});

class ThreadManager extends Component {
    state = {
        threads: [],
        activeThreadIndex: -1,
        value: 0,
        loading: false
    };

    getPosts(url) {
        return new Promise((resolve, reject) => {
            request
                .post('http://localhost:4567/tintin/th')
                .send({url: url})
                .set('Content-Type', 'application/json')
                .end((err, res) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        resolve(JSON.parse(res.text));
                    }
                });
        });
    }

    handleChange = (event, value) => {
        this.setState({ activeThreadIndex: value });
    };

    updateThread = () => {
        if (!this.state.loading) {
            // console.log(this.state.threads[this.state.activeThreadIndex], this.state.threads[this.state.activeThreadIndex].thread.uri);
            this.setState(
                {
                    loading: true
                },
                () => {
                    this.getPosts(this.state.threads[this.state.activeThreadIndex].thread.uri)
                        .then(data => {
                            console.log("data", this.state.threads);
                            this.setState(update(this.state, {
                                threads: {
                                    [this.state.activeThreadIndex]: {
                                        posts: {
                                            $set: data
                                        }
                                    },
                                },
                                loading: {
                                    $set: false
                                }
                                }),
                                () => {
                                    console.log(this.state.threads);
                                });
                        });
                }
            )
        }
    };

    loadThread(nt) {
        if (this.loading) {
            return;
        }
        if (!this.state.threads.find(t => t.uri === nt.uri)) {
            // threads.push(nt);
            this.setState({
                    loading: true
                },
                () => {
                this.getPosts(nt.uri).then(
                        data => {
                            this.setState({
                                threads: this.state.threads.concat({
                                    thread: nt,
                                    posts: data,
                                }),
                                activeThreadIndex: this.state.threads.length,
                                loading: false
                            });
                        }
                    );
                }
            );
        } else {
            this.setState({
                activeThreadIndex: this.state.threads.map(t => t.uri).indexOf(nt.uri)
            });
        }
    }

    render() {
        const { classes } = this.props;
        const { loading } = this.state;
        console.log(this.state.threads);
        const tabs = this.state.threads.map(t => {
            return (<Tab key={t.thread.id} label={t.thread.head_letters} />);
        });
        const content = this.state.activeThreadIndex >= 0 ? this.state.threads[this.state.activeThreadIndex] : {
            posts: []
        };
        console.log("content", content);

        return (
            <div className={classes.root}>
                <div className="Thread-tab">
                    <AppBar position="static" color="default">
                        <Tabs
                            value={this.state.activeThreadIndex}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            scrollable
                            scrollButtons="auto"
                        >
                            {tabs}
                        </Tabs>
                    </AppBar>
                </div>
                <div className={classes.threadContent}>
                    <Thread {...content} />
                </div>
                <div className={classes.wrapper}>
                    {
                    <Button fab color="primary" className={classes.refreshButton} onClick={this.updateThread}>
                        <Refresh />
                    </Button>
                    }
                    {loading && <CircularProgress className={classes.fab} size={42} />}
                </div>
            </div>
        );
    }
}

ThreadManager = withStyles(styles)(ThreadManager);
export default ThreadManager;
